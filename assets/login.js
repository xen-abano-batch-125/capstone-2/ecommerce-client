let loginUserForm = document.querySelector('#loginUserForm');

loginUserForm.addEventListener("submit", (event) => {

	event.preventDefault();

	let email = document.querySelector('#email').value;
	let password = document.querySelector('#password').value;

	//this is optional coz theres already required in the html
	if (email === "" || password === "") {
		alert("Please input required fields.")
	} else {
		fetch("http://localhost:3000/api/users/login",
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					email: email,
					password: password
				})
			}
		) //console.log(result)
		.then( result => result.json())
		.then( result => {

			//save to local storage
			localStorage.setItem("token", result.access);

			let token = result.access
			if (token) {
				fetch("http://localhost:3000/api/users/details", {
					//this is optional coz GET a default
					method: "GET", 
					headers: {
						"Authorization" : `Bearer ${result.access}`
					}
				})
				.then(result=> result.json())
				.then(result => {
					localStorage.setItem("id", result._id);
					localStorage.setItem("isAdmin", result.isAdmin);
					window.location.replace('./products.html');
				})
			}
		})
	}
})