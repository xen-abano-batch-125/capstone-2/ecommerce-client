let params = new URLSearchParams(window.location.search);
let userId = params.get('userId');
console.log(userId);

let token = localStorage.getItem('token');

fetch(`http://localhost:3000/api/users/${userId}/setAsAdmin`,
	{
		method: "PUT",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	}
)
.then(result => result.json())
.then( result => {
	console.log(result)

	if (result) {
		alert(`You are now an admin!`)
		//window.location.replace(`./courses.html`)
	} else {
		alert(`Something went wrong.`);
	}
})