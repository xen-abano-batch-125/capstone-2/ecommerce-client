//target the form
let createProduct= document.querySelector('#createProduct');

//listen to an even (such as submit)
createProduct.addEventListener("submit", (e) => {
	e.preventDefault();

//get the values of the form
let name = document.querySelector('#name').value
let price = document.querySelector('#price').value
let desc = document.querySelector('#desc').value

	if(name !== "" && price !== "" && desc !== ""){
	//use fetch to send the data to the server
		
		let token = localStorage.getItem("token");

		fetch("http://localhost:3000/api/products/addProduct", 
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${token}`
				},
				body: JSON.stringify({
					name: name,
					price: price,
					desc: desc
				})
			}
		)
		.then( result => result.json())
		.then( result => {
			// console.log(result)

			if(result){
				alert("Product Succesfully Created");
				window.location.replace('./products.html')
			} else {
				alert("Product Creation Failed. Something went wrong.")
			}

		})
	}
})
